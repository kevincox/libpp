// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

/** Function Attributes
 * 
 * \file
 * \headerfile pp.h <pp.h>
 * 
 * This file contains macros which can be used to provide the compiler with
 * extra information.  This information can be used for things such as
 * additional warnings and to improve code generation.
 * 
 * The application of these attributes is a best effort process and if the
 * compiler does not support them they will expand to nothing.
 * 
 * None of these attributes change the semantics of the functions (except for
 * possibly adding restrictions on how they can behave) so whether or not they
 * are defined should not affect the correctness of your program.
 * 
 * \note
 * Many of these are easy for the compiler to deduce when it can see the
 * implementation.  However these attributes are very useful when declared on
 * function definitions where the implementation is in a different translation
 * unit.
 */

#include "../pp.h"

#ifndef PP___FUNC_H
#define PP___FUNC_H

//#define PP_FUNC_INLINE inline // Changes meaning of code.

/** Mark that a function will not return.
 * 
 * \def PP_FUNC_NORETURN
 * 
 * This attribute can be used to optimize this function (as exit code need
 * not be generated) and calling functions.  It may also affect warnings as the
 * compiler knows that control will not pass the function call.
 * 
 * \warning
 * If this function returns the behavior is undefined.
 * 
 * \note
 * It doesn't make sense for the function to have a return type other than void.
 * 
 * ~~~{.c}
 * PP_FUNC_NORETURN
 * void assert_not_reached(const char *msg)
 * {
 * 	fprintf(stderr, "FATAL: Unexpected path reached.\n%s", msg);
 * 	abort();
 * }
 * ~~~
 */
#if __STDC_VERSION__ >= 201112L
	// LOG(PP_FUNC_NORETURN => _Noreturn)
#	define PP_FUNC_NORETURN _Noreturn
#	define PP_HAS_FUNC_NORETURN 1
#elif PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(2,5)
	// LOG(PP_FUNC_NORETURN => __attribute__((noreturn)) (gcc))
#	define PP_FUNC_NORETURN __attribute__((__noreturn__))
#	define PP_HAS_FUNC_NORETURN 1
#elif _pp_CLANG_HAS_ATTR(__noreturn__)
	// LOG(PP_FUNC_NORETURN => __attribute__((noreturn)) (clang))
#	define PP_FUNC_NORETURN __attribute__((__noreturn__))
#	define PP_HAS_FUNC_NORETURN 1
#elif _MSC_VER >= 1310
	// LOG(PP_FUNC_NORETURN => _declspec(noreturn))
#	define PP_FUNC_NORETURN __declspec(noreturn)
#	define PP_HAS_FUNC_NORETURN 1
#else
	// LOG(PP_FUNC_NORETURN => none)
#	define PP_FUNC_NORETURN /* not supported */
#	define PP_HAS_FUNC_NORETURN 0
#endif

/** Mark the parameter that determines size of returned memory.
 * 
 * \def PP_FUNC_RETSIZE
 * 
 * This attribute specifies that the return value is a pointer to memory who's
 * size is given by the argument (if one given) or the product of the arguments.
 * 
 * ~~~{.c}
 * PP_FUNC_RETSIZE(1)
 * void *mymalloc(size_t size);
 * ~~~
 * 
 * ~~~{.c}
 * PP_FUNC_RETSIZE(1,2)
 * void *mycalloc(size_t count, size_t size)
 * {
 * 	return calloc(count, size);
 * }
 * ~~~
 * 
 * \param  a1 The index of the size argument.
 * \param  a2 If given, \a a1 and \a a2 are multiplied to get the returned size.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(4,3)
#	define PP_FUNC_RETSIZE(a1, a2...) __attribute__((__alloc_size__(a1, ##a2)))
#	define PP_HAS_FUNC_RETURNSIZE 1
#elif _pp_CLANG_HAS_ATTR(__alloc_size__)
#	define PP_FUNC_RETSIZE(a1, a2...) __attribute__((__alloc_size__(a1, ##a2)))
#	define PP_HAS_FUNC_RETURNSIZE 1
#else
#	define PP_FUNC_RETSIZE(a1, ...) /* not supported */
#	define PP_HAS_FUNC_RETURNSIZE 0
#endif

/** Constant Function.
 * 
 * \def PP_FUNC_CONST
 * 
 * A constant function is a function who's only affect is it's return value and
 * the return value is based only on the value of it's arguments.
 * 
 * \warning
 * A constant function **may not** follow pointers in it's arguments.
 * for a function that can examine global memory see PP_FUNC_PURE.
 * 
 * ~~~{.c}
 * PP_FUNC_CONST
 * int square(int x)
 * {
 * 	return x*x;
 * }
 * ~~~
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(2,5)
#	define PP_FUNC_CONST __attribute__((__const__))
#	define PP_HAS_FUNC_CONST 1
#elif _pp_CLANG_HAS_ATTR(__const__)
#	define PP_FUNC_CONST __attribute__((__const__))
#	define PP_HAS_FUNC_CONST 1
#else
#	define PP_FUNC_CONST /* Not available. */
#	define PP_HAS_FUNC_CONST 0
#endif

/** Mark a function as pure.
 * 
 * \def PP_FUNC_PURE
 * 
 * Pure functions have no side affects and their return value depends only on
 * their argument and global memory.
 * 
 * ~~~{.c}
 * PP_FUNC_PURE
 * Fraction frac_mul(Fraction *a, Fraction *b)
 * {
 * 	return (Fraction){.num = a.num*b.num, .den = a.den*b.den};
 * }
 * ~~~
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(2,96)
#	define PP_FUNC_PURE __attribute__((__pure__))
#	define PP_HAS_FUNC_PURE 1
#elif _pp_CLANG_HAS_ATTR(__pure__)
#	define PP_FUNC_PURE __attribute__((__pure__))
#	define PP_HAS_FUNC_PURE 1
#elif _MSC_VER >= 1400
#	define PP_FUNC_PURE __declspec(noalias)
#	define PP_HAS_FUNC_PURE 1
#else
#	define PP_FUNC_PURE /* Not available. */
#	define PP_HAS_FUNC_PURE 0
#endif

/** Mark a function as deprecated.
 * 
 * \def PP_FUNC_DEPRECATED_STR
 * 
 * Many compilers will produce warnings when using deprecated functions.
 * 
 * \param  msg The free-form depreciation message string.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,1)
	#define PP_FUNC_DEPRECATED_STR(msg) __attribute__((__deprecated__(msg)))
	#define PP_HAS_FUNC_DEPRECATED 1
#elif _pp_CLANG_HAS_ATTR(__deprecated__)
	#define PP_FUNC_DEPRECATED_STR(msg) __attribute__((__deprecated__(msg)))
	#define PP_HAS_FUNC_DEPRECATED 1
#elif _MSC_VER >= 1400
	#define PP_FUNC_DEPRECATED_STR(msg) __declspec(deprecated(msg))
	#define PP_HAS_FUNC_DEPRECATED 1
#elif _MSC_VER
	#define PP_FUNC_DEPRECATED_STR(msg) __declspec(deprecated)
	#define PP_HAS_FUNC_DEPRECATED 1
#else
	#define PP_FUNC_DEPRECATED_STR(msg) /* Not available. */
	#define PP_HAS_FUNC_DEPRECATED 0
#endif

/** Mark a function depreciated.
 * 
 * \see PP_FUNC_DEPRECATED_STR
 * 
 * A convenience macro that stringifies it's arguments.  It is useful for when
 * you don't need complete control of the formatting.
 * 
 * ~~~{.c}
 * PP_FUNC_DEPRECATED(Use my_new_fun() instead.  It doesn't kill kittens.)
 * void my_old_func(void);
 * ~~~
 */
#define PP_FUNC_DEPRECATED(...) PP_FUNC_DEPRECATED_STR(#__VA_ARGS__)

/** Mark a printf-style format string.
 * 
 * \def PP_FUNC_PRINTF
 * 
 * Identify a format string and list of arguments like those used by the printf
 * family of functions.  Some compilers may use this information to warn about
 * type mismatches.
 * 
 * ~~~{.c}
 * PP_FUNC_PRINTF(2,3)
 * void log(LogLevel level, const char *fmt, ...);
 * ~~~
 * 
 * \param  format The (1-based) argument index of the format string.
 * \param  first The index of the first value.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,3)
#	define PP_FUNC_PRINTF(fmt, arg) __attribute__((__format__(__printf__,fmt,arg)))
#	define PP_HAS_FUNC_PRINTF 1
#elif _pp_CLANG_HAS_ATTR(__format__)
#	define PP_FUNC_PRINTF(fmt, arg) __attribute__((__format__(__printf__,fmt,arg)))
#	define PP_HAS_FUNC_PRINTF 1
#else
#	define PP_FUNC_PRINTF(format, first) /* Not available. */
#	define PP_HAS_FUNC_PRINTF 0
#endif


/** Mark a scanf-style format string.
 * 
 * \def PP_FUNC_SCANF
 * 
 * Identify a format string and list of arguments like those used by the scanf
 * family of functions.  Some compilers may use this information to warn about
 * type mismatches.
 * 
 * ~~~{.c}
 * PP_FUNC_SCANF(2,3)
 * void log(LogLevel level, const char *fmt, ...);
 * ~~~
 * 
 * \param  format The (1-based) argument index of the format string.
 * \param  first The index of the first value.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,3)
#	define PP_FUNC_SCANF(fmt, arg) __attribute__((__format__(__scanf__,fmt,arg)))
#	define PP_HAS_FUNC_PRINTF 1
#elif _pp_CLANG_HAS_ATTR(__format__)
#	define PP_FUNC_SCANF(fmt, arg) __attribute__((__format__(__scanf__,fmt,arg)))
#	define PP_HAS_FUNC_PRINTF 1
#else
#	define PP_FUNC_SCANF(format, first) /* Not available. */
#	define PP_HAS_FUNC_SCANF 0
#endif

/** Mark a function as returning restricted memory.
 * 
 * \def PP_FUNC_NEWMEM
 * 
 * This can be used to mark that a function returns "fresh" memory.  This
 * returned address (if non-NULL) does not alias any other pointers and the
 * memory does not contain references to any valid objects.
 * 
 * \warning
 * The result is undefined if any of these conditions are violated.
 * 
 * ~~~{.c}
 * PP_FUNC_NEWMEM
 * Object *object_new(void)
 * {
 * 	return malloc(sizeof(Object));
 * }
 * ~~~
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,0)
#	define PP_FUNC_NEWMEM __attribute__((__malloc__))
#	define PP_HAS_FUNC_NEWMEM 1
#elif _pp_CLANG_HAS_ATTR(__malloc__)
#	define PP_FUNC_NEWMEM __attribute__((__malloc__))
#	define PP_HAS_FUNC_NEWMEM 1
#elif _MSC_VER >= 1400
	// Not a perfect match but a subset, so may as well add it.
#	define PP_FUNC_NEWMEM __declspec(restrict)
#	define PP_HAS_FUNC_NEWMEM 1
#else
#	define PP_FUNC_NEWMEM /* Not available. */
#	define PP_HAS_FUNC_NEWMEM 0
#endif

/** Mark an argument as non-null.
 * 
 * \def PP_FUNC_NONNULL
 * 
 * Specify arguments that can not be passed NULL pointers.  The compiler may
 * generate warnings or optimize the code knowing that the argument will never
 * be null.
 * 
 * \warning
 * It is undefined behavior to pass a NULL pointer in an attribute marked
 * non-null.
 * 
 * ~~~{.c}
 * PP_FUNC_NONNULL(1,2)
 * int strcmp(const char *a, const char *b);
 * ~~~
 * 
 * \param i... The (1-based) index of a non-null arguments.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,3)
#	define PP_FUNC_NONNULL(i, ...) __attribute__((__nonnull__(i, ##__VA_ARGS__)))
#	define PP_HAS_FUNC_NONNULL 1
#elif _pp_CLANG_HAS_ATTR(__nonnull__)
#	define PP_FUNC_NONNULL(i, ...) __attribute__((__nonnull__(i, ##__VA_ARGS__)))
#	define PP_HAS_FUNC_NONNULL 1
#else
#	define PP_FUNC_NONNULL /* Not available. */
#	define PP_HAS_FUNC_NONNULL 0
#endif

/** Mark a function never returning NULL.
 * 
 * \def PP_FUNC_RETNONNULL
 * 
 * \warning
 * It is undefined behaviour if this function returns NULL.
 * 
 * ~~~{.c}
 * PP_FUNC_RETNONNULL
 * void *safemalloc(size_t size)
 * {
 * 	void *r = malloc(size);
 * 	if (!r) abort();
 * 	return r;
 * }
 * ~~~
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(4,9)
#	define PP_FUNC_RETNONNULL __attribute__((__returns_nonnull__))
#	define PP_HAS_FUNC_NONNULLRET 1
#elif _pp_CLANG_HAS_ATTR(__returns_nonnull__)
#	define PP_FUNC_RETNONNULL __attribute__((__returns_nonnull__))
#	define PP_HAS_FUNC_NONNULLRET 1
#else
#	define PP_FUNC_RETNONNULL /* Not available. */
#	define PP_HAS_FUNC_NONNULLRET 0
#endif

/** Mark that a function does not throw exceptions.
 * 
 * \def PP_FUNC_NOTHROW
 * 
 * \warning
 * It is undefined behaviour if the function throws an exception.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,3)
#	define PP_FUNC_NOTHROW __attribute__((__nothrow__))
#	define PP_HAS_FUNC_NOTHROW 1
#elif _pp_CLANG_HAS_ATTR(__nothrow__)
#	define PP_FUNC_NOTHROW __attribute__((__nothrow__))
#	define PP_HAS_FUNC_NOTHROW 1
#elif _MSC_VER
#	define PP_FUNC_NOTHROW __declspec(nothrow)
#	define PP_HAS_FUNC_NOTHROW 1
#else
#	define PP_FUNC_NOTHROW /* Not available. */
#	define PP_HAS_FUNC_NOTHROW 0
#endif

/** Mark a function as hot.
 * 
 * \def PP_FUNC_HOT
 * 
 * Hot functions are called a lot.  Compilers can use this information to
 * optimize these functions more aggressively as well as grouping them together
 * to improve locality.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(4,3)
#	define PP_FUNC_HOT __attribute__((__hot__))
#	define PP_HAS_FUNC_HOT 1
#elif _pp_CLANG_HAS_ATTR(__hot__)
#	define PP_FUNC_HOT __attribute__((__hot__))
#	define PP_HAS_FUNC_HOT 1
#else
#	define PP_FUNC_HOT /* Not available. */
#	define PP_HAS_FUNC_HOT 0
#endif

/** Mark a function as cold.
 * 
 * \def PP_FUNC_COLD
 * 
 * Cold functions are rarely called.  Compilers can use this information to
 * optimize for size over speed and group cold functions together.  Compilers
 * may also mark paths that call cold functions as cold to improve branch
 * prediction and other probabilistic optimizations.
 * 
 * ~~~{.c}
 * PP_FUNC_COLD
 * void _warn(const char *__FILE__, size_t line, const char *fmt, ...);
 * #define warn(...) _warn(__FILE__, __LINE__, ...)
 * ~~~
 * 
 * Assuming the above definition.
 * 
 * ~~~{.c}
 * if (x) warn("x is %d\n", x);
 * ~~~
 * 
 * The compiler can now assume that x is usually false.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(4,3)
#	define PP_FUNC_COLD __attribute__((__cold__))
#	define PP_HAS_FUNC_COLD 1
#elif _pp_CLANG_HAS_ATTR(__cold__)
#	define PP_FUNC_COLD __attribute__((__cold__))
#	define PP_HAS_FUNC_COLD 1
#else
#	define PP_FUNC_COLD /* Not available. */
#	define PP_HAS_FUNC_COLD 0
#endif

/** Mark a function that returns multiple times.
 * 
 * \def PP_FUNC_MULTIRET
 * 
 * An example of a function that returns multiple times is `setjmp()`.
 */
#if PP_COMPILER_GCC_REALLY
#	define PP_FUNC_MULTIRET __attribute__((__returns_twice__))
#	define PP_HAS_FUNC_MULTIRET 1
#elif PP_COMPILER_CLANG
#	define PP_FUNC_MULTIRET __attribute__((__returns_twice__))
#	define PP_HAS_FUNC_MULTIRET 1
#else
#	define PP_FUNC_MULTIRET /* Not available. */
#	define PP_HAS_FUNC_MULTIRET 0
#endif

/** Mark a function that is NULL-terminated.
 * 
 * \def PP_FUNC_NULLTERM
 * 
 * This function is useful for vararg functions who determine the end of their
 * arguments list with a NULL argument.  The compiler can use this to generate
 * a warning if an explicit NULL terminator is not provided.
 * 
 * ~~~{.c}
 * PP_FUNC_NULLTERM(0)
 * void free_all(...);
 * 
 * free_all(p1, p2, p3, NULL);
 * ~~~
 * 
 * \param o The offset from the end of the list.  This is usually 0.
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(4,0)
#	define PP_FUNC_NULLTERM __attribute__((__sentinel__))
#	define PP_HAS_FUNC_NULLTERM 1
#elif _pp_CLANG_HAS_ATTR(__sentinel__)
#	define PP_FUNC_NULLTERM __attribute__((__sentinel__))
#	define PP_HAS_FUNC_NULLTERM 1
#else
#	define PP_FUNC_NULLTERM /* Not available. */
#	define PP_HAS_FUNC_NULLTERM 0
#endif

/** Mark a return value as important.
 * 
 * \def PP_FUNC_RETIMPORTANT
 * 
 * The return value of this function must always be used.  Compilers may
 * produce an error if the value is discarded.
 * 
 * ~~~{.c}
 * PP_FUNC_RETIMPORTANT
 * void malloc(size_t size);
 */
#if PP_COMPILER_GCC_REALLY && PP_VER_GCC_GTE(3,3)
#	define PP_FUNC_RETIMPORTANT __attribute__((__warn_unused_result__))
#	define PP_HAS_FUNC_RETIMPORTANT 1
#elif _pp_CLANG_HAS_ATTR(__warn_unused_result__)
#	define PP_FUNC_RETIMPORTANT __attribute__((__warn_unused_result__))
#	define PP_HAS_FUNC_RETIMPORTANT 1
#else
#	define PP_FUNC_RETIMPORTANT /* Not available. */
#	define PP_HAS_FUNC_RETIMPORTANT 0
#endif

#endif
