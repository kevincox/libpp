// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "../pp.h"

#ifndef PP___VERSION_H
#define PP___VERSION_H

/** Check is version requirement is satisfied.
 * 
 * Checks if desired version is satisfied by the available version using semver
 * requirements (majors must be the same and available minor must be greater
 * than or equal to desired minor).
 * 
 * \param  amajor Available major version.
 * \param  aminor Available minor version.
 * \param  dmajor Desired major version.
 * \param  dminor Desired minor version.
 * \return 1 iff version matches, otherwise 0.
 */
#define PP_VER_MATCH(amajor, aminor, dmajor, dminor) \
	((amajor)==(dmajor) && (aminor)>=(dminor))

/** Check if version is greater than or equal to.
 *
 * Checks of the available version is greater than or equal to the desired
 * version where the major takes precedence over the minor.
 * 
 * \param  amajor Available major version.
 * \param  aminor Available minor version.
 * \param  dmajor Desired major version.
 * \param  dminor Desired minor version.
 * \return 1 iff version is >=, otherwise 0.
 */
#define PP_VER_GTE(amajor, aminor, dmajor, dminor) \
	((amajor)>(dmajor) || (amajor)==(dmajor)&&(aminor)>=(dminor))

/** Check if using compatible of GCC
 * 
 * \param  dmajor The desired major version.
 * \param  dminor The desired minor version.
 * \return 1 iff using GCC and version matches, otherwise 0.
 */
#define PP_VER_GCC_MATCH(dmajor, dminor) \
	defined(__GNUC__) && PP_VER_MATCH(__GNUC__,__GNUC_MINOR__, dmajor,dminor)

/** Check if using greater then or equal version of GCC.
 * 
 * \param dmajor The desired major version.
 * \param dminor The desired minor version.
 * \return 1 iff using GCC and version is >=, otherwise 0.
 */
#define PP_VER_GCC_GTE(dmajor, dminor) \
	defined(__GNUC__) && PP_VER_GTE(__GNUC__,__GNUC_MINOR__, dmajor,dminor)

#endif
