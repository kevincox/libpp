// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "../pp.h"

#ifndef PP___ENV_H
#define PP___ENV_H

/** Is the compiler Clang?
 * 
 * \return True iff the compiler is clang.
 */
#define PP_COMPILER_CLANG defined(__clang__)

/** Is the compiler Intel CC?
 * 
 * \return True iff the compiler is clang.
 */
#define PP_COMPILER_ICC defined(__INTEL_COMPILER)

/** Is the compiler GCC?
 * 
 * \warning Some compilers (like Clang) pretend to be GCC.  If you want to
 * ensure that you are actually using GCC see PP_COMPILER_GCC_REALLY
 * 
 * \return True iff the compiler appears to be GCC.
 */
#define PP_COMPILER_GCC defined(__GNUC__)

/** Is the compiler really GCC?
 * 
 * \see PP_COMPILER_GCC
 * 
 * Like PP_COMPILER_GCC but tries to weed out compilers that pretend to be GCC.
 * 
 * \return True iff as far as we know the compiler is GCC.
 */
#define PP_COMPILER_GCC_REALLY (PP_COMPILER_GCC   && \
                               !PP_COMPILER_CLANG && \
                               !PP_COMPILER_ICC)

#ifdef __has_attribute
#	define _pp_CLANG_HAS_ATTR(attr) __has_attribute(attr)
#else
#	define _pp_CLANG_HAS_ATTR(attr) 0
#endif

#endif
