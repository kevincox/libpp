// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "../pp.h"

#ifndef PP___LOOP_H
#define PP___LOOP_H

#define PP_FIRST(a, ...) a

#define PP_EMPTY(...) _pp_EMPTY2(__VA_ARGS__)
#define _pp_EMPTY2(...) PP_NOT(PP_FIRST(_pp_EMPTY_TRUE##__VA_ARGS__,1))
#define _pp_EMPTY_TRUE _pp_EMPTY_VAL
#define _pp_EMPTY_VAL 0

#define PP_ARGC(...) PP_IF(PP_EMPTY(__VA_ARGS__), 0, _pp_ARGC2(__VA_ARGS__))
#define _pp_ARGC2(...) _pp_ARGC3(__VA_ARGS__,99,98,97,96,95,94,93,92,91,90, \
                                             89,88,87,86,85,84,83,82,81,80, \
                                             79,78,77,76,75,74,73,72,71,70, \
                                             69,68,67,66,65,64,63,62,61,60, \
                                             59,58,57,56,55,54,53,52,51,50, \
                                             49,48,47,46,45,44,43,42,41,40, \
                                             39,38,37,36,35,34,33,32,31,30, \
                                             29,28,27,26,25,24,23,22,21,20, \
                                             19,18,17,16,15,14,13,12,11,10, \
                                              9, 8, 7, 6, 5, 4, 3, 2, 1, 0,)
#define _pp_ARGC3(_00,_01,_02,_03,_04,_05,_06,_07,_08,_09, \
                  _10,_11,_12,_13,_14,_15,_16,_17,_18,_19, \
                  _20,_21,_22,_23,_24,_25,_26,_27,_28,_29, \
                  _30,_31,_32,_33,_34,_35,_36,_37,_38,_39, \
                  _40,_41,_42,_43,_44,_45,_46,_47,_48,_49, \
                  _50,_51,_52,_53,_54,_55,_56,_57,_58,_59, \
                  _60,_61,_62,_63,_64,_65,_66,_67,_68,_69, \
                  _70,_71,_72,_73,_74,_75,_76,_77,_78,_79, \
                  _80,_81,_82,_83,_84,_85,_86,_87,_88,_89, \
                  _90,_91,_92,_93,_94,_95,_96,_97,_98,_99, ...) _99

#endif
