// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "../pp.h"

#ifndef PP___UTIL_H
#define PP___UTIL_H

#define PP_CAT(a, b) _pp_cat(a, b)
#define _pp_cat(a, b) a##b

#define PP_APPLY(f, a) f a

#define PP_PRAGMA(...) _PP_PRAGMA_STR(__VA_ARGS__)
#define PP_HAS_PRAGMA PP_HAS_PRAGMA_STR

#if __STDC_VERSION__ >= 201112L
#	define PP_PRAGMA_STR(...) _Pragma(#__VA_ARGS__)
#	define PP_HAS_PRAGMA_STR 1
#else
#	define PP_PRAGMA_STR(...) /* Not available. */
#	define PP_HAS_PRAGMA_STR 0
#endif

#define PP_LOG(...)       PP_LOG_STR(#__VA_ARGS__)
#define PP_LOG_STR(msg)   PP_PRAGMA(message msg)
#define PP_ERROR(...)     PP_ERROR_STR(#__VA_ARGS__)
#define PP_ERROR_STR(msg) PP_PRAGMA(error msg)

#endif
