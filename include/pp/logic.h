// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#ifndef PP___LOGIC_H
#define PP___LOGIC_H

#include "util.h"

/** Convert value to boolean.
 * 
 * If the argument is 0 return 0, for any other value it returns 1.
 * 
 * \param  x The value to convert.
 * \return 1 iff x is not `0`.
 */
#define PP_BOOL(x) PP_APPLY(_pp_BOOL_CHECK, (PP_CAT(_pp_BOOL_VAL_, x), 1))
#define _pp_BOOL_VAL_0 _, 0
#define _pp_BOOL_CHECK(_, v, ...) v

/** Negate argument.
 * 
 * \param  x Value to negate, coerced to boolean.
 * \return 1 iff \a x was 0.
 */
#define PP_NOT(x) PP_CAT(_pp_NOT_, PP_BOOL(x))
#define _pp_NOT_0 1
#define _pp_NOT_1 0

/** Expand if true.
 * 
 * Expands to the variable arguments if cond is true.
 * 
 * \param  cond  Condition, coerced to boolean.
 * \param  ... What to expand to.
 * \return ... or nothing.
 */
#define PP_IFTRUE(cond, ...) \
	PP_CAT(_pp_IFTRUE_, PP_BOOL(cond))(__VA_ARGS__)
#define _pp_IFTRUE_0(...) /* nothing */
#define _pp_IFTRUE_1(...) __VA_ARGS__

/** Expand if false.
 * 
 * `PP_IFFALSE(foo, bar)` is equivalent to `PP_IFTRUE(PP_NOT(foo), bar)`.
 */
#define PP_IFFALSE(cond, ...) PP_IFTRUE(PP_NOT(cond), __VA_ARGS__)

/** Select based on condition.
 * 
 * \param  cond The condition, coerced to boolean.
 * \param  ift The true value.
 * \param  iff The false value.
 * \return \a ift if \a cond else \a iff.
 */
#define PP_IF(cond, ift, iff) PP_IFTRUE(cond, ift) PP_IFFALSE(cond, iff)
#define _pp_IFTRUE_0(...) /* nothing */
#define _pp_IFTRUE_1(...) __VA_ARGS__

#endif
