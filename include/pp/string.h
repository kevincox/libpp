// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "../pp.h"

#ifndef PP___STRING_H
#define PP___STRING_H

#define _pp_STR(...) #__VA_ARGS__

/** Stringy argument.
 * 
 * Returns the argument represented as a string after expansion.  If you want
 * the string before expansion use `#`.
 */
#define PP_STR(...) _pp_STR(__VA_ARGS__)

/** The date as a string. */
#define PP_DATE __DATE__

/** The time as a string. */
#define PP_TIME __TIME__

/** The current date and time as a string. */
#define PP_DATETIME PP_DATE PP_TIME

/** The current function as a string. */

#endif
