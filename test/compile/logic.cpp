// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "common.h"

TEST(logic, PP_BOOL)
{
	EXPECT_EXPAND(0, PP_BOOL(0));
	EXPECT_EXPAND(1, PP_BOOL(1));
	EXPECT_EXPAND(1, PP_BOOL(14));
	EXPECT_EXPAND(1, PP_BOOL(12141135));
	EXPECT_EXPAND(1, PP_BOOL(nottrue));
}

TEST(logic, PP_IFTRUE)
{
	EXPECT_EXPAND(foobar, PP_IFTRUE(1, foobar));
	EXPECT_EXPAND((1,2,3), (PP_IFTRUE(1, 1,2,3)));
	EXPECT_EXPAND(, PP_IFTRUE(0, foobar));
	EXPECT_EXPAND(, PP_IFTRUE(0, 1,2,3));
}

TEST(logic, PP_IFFALSE)
{
	EXPECT_EXPAND(, PP_IFTRUE(0, foobar));
	EXPECT_EXPAND(, PP_IFTRUE(0, 1,2,3));
	EXPECT_EXPAND(foobar, PP_IFFALSE(0, foobar));
	EXPECT_EXPAND((1,2,3), (PP_IFFALSE(0, 1,2,3)));
}

TEST(PP_IF, c)
{
	EXPECT_EXPAND(a, PP_IF(1, a, b));
	EXPECT_EXPAND(1, PP_IF(2, 1, 0));
	EXPECT_EXPAND(b, PP_IF(0, a, b));
}
