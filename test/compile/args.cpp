// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "common.h"

#include <pp.h>

TEST(args, PP_EMPTY)
{
	EXPECT_EXPAND(1, PP_EMPTY());
	EXPECT_EXPAND(0, PP_EMPTY(1));
	EXPECT_EXPAND(0, PP_EMPTY(1,2));
	EXPECT_EXPAND(0, PP_EMPTY(1,2,3));
}

TEST(args, PP_ARGC)
{
	EXPECT_EXPAND(0, PP_ARGC());
	EXPECT_EXPAND(1, PP_ARGC(1));
	EXPECT_EXPAND(2, PP_ARGC(1,2));
	EXPECT_EXPAND(3, PP_ARGC(1,2,3));
	EXPECT_EXPAND(100, PP_ARGC( 1, 2, 3, 4, 5, 6, 7, 8, 9,10,
	                           11,12,13,14,15,16,17,18,19,20,
	                           21,22,23,24,25,26,27,28,29,30,
	                           31,32,33,34,35,36,37,38,39,40,
	                           41,42,43,44,45,46,47,48,49,50,
	                           51,52,53,54,55,56,57,58,59,60,
	                           61,62,63,64,65,66,67,68,69,70,
	                           71,72,73,74,75,76,77,78,79,80,
	                           81,82,83,84,85,86,87,88,89,90,
	                           91,92,93,94,95,96,97,98,99,100));
}
