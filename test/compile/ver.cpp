// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include "common.h"

TEST(PP_VER_MATCH, c)
{
	EXPECT_TRUE (PP_VER_MATCH(1,0, 1,0));
	EXPECT_TRUE (PP_VER_MATCH(1,1, 1,0));
	EXPECT_FALSE(PP_VER_MATCH(1,0, 1,1));
	EXPECT_FALSE(PP_VER_MATCH(1,0, 2,0));
	EXPECT_FALSE(PP_VER_MATCH(2,0, 1,0));
}

TEST(PP_VER_MATCH, pp)
{
#if PP_VER_MATCH(1,0, 1,0)
	SUCCEED()
#else
	ADD_FAILURE()
#endif
		<< "PP_VER_MATCH(1,0, 1,0)";

#if PP_VER_MATCH(1,1, 1,0)
	SUCCEED()
#else
	ADD_FAILURE()
#endif
		<< "PP_VER_MATCH(1,1, 1,0)";

#if PP_VER_MATCH(1,0, 1,1)
	ADD_FAILURE()
#else
	SUCCEED()
#endif
		<< "PP_VER_MATCH(1,0, 1,1)";

#if PP_VER_MATCH(1,0, 2,0)
	ADD_FAILURE()
#else
	SUCCEED()
#endif
		<< "PP_VER_MATCH(1,0, 2,0)";

#if PP_VER_MATCH(2,0, 1,0)
	ADD_FAILURE()
#else
	SUCCEED()
#endif
		<< "PP_VER_MATCH(2,0, 1,0)";
}

TEST(PP_VER_GTE, c)
{
	EXPECT_TRUE (PP_VER_GTE(1,0, 1,0));
	EXPECT_TRUE (PP_VER_GTE(1,1, 1,0));
	EXPECT_TRUE (PP_VER_GTE(2,0, 1,0));
	EXPECT_FALSE(PP_VER_GTE(1,0, 1,1));
	EXPECT_FALSE(PP_VER_GTE(1,0, 2,0));
}
