// Copyright 2014 Kevin Cox

/*******************************************************************************
*                                                                              *
*  This software is provided 'as-is', without any express or implied           *
*  warranty. In no event will the authors be held liable for any damages       *
*  arising from the use of this software.                                      *
*                                                                              *
*  Permission is granted to anyone to use this software for any purpose,       *
*  including commercial applications, and to alter it and redistribute it      *
*  freely, subject to the following restrictions:                              *
*                                                                              *
*  1. The origin of this software must not be misrepresented; you must not     *
*     claim that you wrote the original software. If you use this software in  *
*     a product, an acknowledgment in the product documentation would be       *
*     appreciated but is not required.                                         *
*                                                                              *
*  2. Altered source versions must be plainly marked as such, and must not be  *
*     misrepresented as being the original software.                           *
*                                                                              *
*  3. This notice may not be removed or altered from any source distribution.  *
*                                                                              *
*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "../../include/pp.h"

PP_FUNC_NORETURN
void doesnt_return(void)
{
	exit(0);
}

PP_FUNC_RETURNSIZE(1)
void *my_malloc(size_t s)
{
	return malloc(s);
}

PP_FUNC_RETURNSIZE(1,2)
void *my_calloc(size_t count, size_t size)
{
	return calloc(count, size);
}

PP_FUNC_CONST
PP_FUNC_DEPRECATED(Use the MAX macro)
int max(int a, int b)
{
	return a > b ? a : b;
}

//PP_FUNC_PURE
size_t my_strlen(char *s)
{
	size_t r = 0;
	while (*s++)
		r++;
	
	return r;
}

//PP_FUNC_ALLOC
void *safe_malloc(void)
{
	void *r = malloc(42);
	if (!r) perror("safe_malloc");
	return r;
}

int main(void)
{
	return 0;
}
